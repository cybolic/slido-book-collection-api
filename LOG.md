# Task

Create a simple RESTful API server in Node.js for the book collection app.
Each book has a title, description, and 1-n authors. Created API endpoints
should allow search in collection and book management.

## Implementation details:
* App should support standard CRUD ops
* We prefer Typescript
* Tools, Framework, and Database are up to you, but you have to write short why? behind your decision


# Initial assessment

* Scope: I'll assume this is a multi-user API and not a personal collection manager.
* Node: Focus on REST only; This already means that larger frameworks like Express are out.
* Node: No-framework is certainly possible, but not good for maintainability; disregard here.
* DB: Scale is not indicated, so let's just assume it will connect to MySQL (Amz Aurora) and that scaling is a DevOps problem. Connection will actually be using a local database.
* Security: I'll assume that the server process is behind an authentication proxy that sets a header `API-Client` to JWT (or similar) payload value `UUID`, being the currently authenticated user's UUID.
* Logging: Log to stdout and let fictional cron processes handle where it should go from there
* REST: The world has settled on plural schemas, so follow suit

## Endpoints:

| Area                         | Method | Route                                   | Description                                             | Get User ID from Auth proxy | For visitor views (links/sharing/browsing) |
|------------------------------|--------|-----------------------------------------|---------------------------------------------------------|-----------------------------|--------------------------------------------|
| User / Book ~~/ Collection~~ | POST   |                                         |                                                         | x                           |                                            |
|                              | GET    |                                         |                                                         | x                           |                                            |
|                              | PATCH  |                                         |                                                         | x                           |                                            |
|                              | DELETE |                                         |                                                         | x                           |                                            |
| Search                       | GET    | /books/:bookId                          | single book                                             | x                           |                                            |
|                              | GET    | /books?                                 | all/queried books on the platform                       | x                           |                                            |
|                              | GET    | /authors/:authorId                      | single author on the platform                           | x                           |                                            |
|                              | GET    | /authors?                               | single/all/queried authors on the platform              | x                           |                                            |
|                              | GET    | /user/books?                            | all/queried books for current user                      | x                           |                                            |
| Not in spec                  | GET    | ~~/user/collections~~                   | all collections of current user                         | x                           |                                            |
| Not in spec                  | GET    | ~~/user/collections/:collection~~       | all books in `collection` of current user               | x                           |                                            |
|                              | GET    | /users/:username/books?                 | all/queried books for user `username` - public username |                             | x                                          |
|                              | GET    | /users/:username/collection/:collection | all collections of user `username`    - public username |                             | x                                          |

* Data structure:
  * User:
    * uuid - let email / password be handled by authentication proxy
    * username
  * Book:
    * uuid
    * title
    * description
    * authors
  * Authors:
    * uuid
    * firstName
    * lastName
  * ~~Collections:~~
    * ~~bookId~~
    * ~~userId~~
* Ideas:
  * Connect to Open Library Books API - this should be client-side, so not really relevant here

```
erDiagram
  BOOK }|--|{ AUTHOR : by
  BOOK {
    string uuid
    string title
    string description
  }
  AUTHOR {
    string firstName
    string lastName
  } 
  USER }o..o{ BOOK : has
  USER {
    string uuid
    string username
  }
```

## Tech stack

| Function      | Choice  | Why                                                                                                                                                                                                                                                                          |
|---------------|---------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Framework     | Koa     | Not as wide-spread as Express, nor as focused as Restify, but hits a good middle-point due to large user base and recent code                                                                                                                                                |
| Database      | TypeORM | An ORM is more likely than raw SQL in a team project (which I'll assume this project emulates) and TypeORM matches well with the Typescript requirement. Also: [Aurora driver for our fictional use-case](https://github.com/ArsenyYankovsky/typeorm-aurora-data-api-driver) |
| Docs / Routes | OpenAPI | Low documentation, can be used to integrate with AWS. `koa-oai-router` seems the choice closest to vanilla OpenAPI, but it's development has stagnated; `koa-openapi` will be used                                                                                           |
| Testing       | Jest    | Widely used, well-supported                                                                                                                                                                                                                                                  |


# Plan

## 04-22 Thu
| Task         | Items      |
|--------------|------------|
| [x] Planning | Everything |

## 04-23 Fri
| Task            | Items                                   |
|-----------------|-----------------------------------------|
| [x] Ground work | Typescript, TypeORM, Koa, OpenAPI, Jest |

Used sqlite3 for the permanent database and sql.js for testing.
This should provide a bit of a wider test base to ensure we don't accidentally do anything that's not database impartial.

## 04-24 Sat
| Task                  | Items           |
|-----------------------|-----------------|
| [x] OpenAPI           | User            |
| [x] Unit tests        | User model      |
| [x] Integration tests | User controller |

## 04-25 Sun
| Task                  | Items           |
|-----------------------|-----------------|
| [x] Controllers       | User controller |
| [x] Unit tests        | Book, Author    |
| [x] Model             | Book, Author    |
| [x] Integration tests | Book, Author    |

## 04-26 Mon
| Task            | Items        |
|-----------------|--------------|
| [x] Controllers | Book, Author |

## 04-27 Tue
Buffer: Moving some routes around. I had multiple user collections in mind originally and that's not in spec.

## 04-28 Wed
Delivery: A bit of touch-up on some of the models and OpenAPI definitions.

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: [
    '<rootDir>/src/tests'
  ],
  testPathIgnorePatterns: [
    '<rootDir>/src/tests/_.*'
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/tests/_.*'
  ]
};

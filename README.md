# A simple RESTful API server in Node.js for the book collection app

## Written using:

* **Koa**
  Because it's popular, more modern than Express and it's been a while since I've looked at it. My normal choice would probably have been Restify here.
* **TypeORM**
  Because it fits well with the Typescript request and I haven't touched it at all, so it's a good learning opportunity. Normally, I would have used raw SQL with possibly a small wrapper.
* **Koa-OpenAPI**
  It's maintained and on its way to support OpenAPI 3. The layout is a bit different to what I was expecting, but it works well enough for this project. I would normally probably have used the standard OpenAPI libraries.
* **Jest**
  Everyone loves Jest.

A diary, written half-free-form, is available in [LOG.md](LOG.md) so you can follow my thought process through this project.


## Running it

**Server**
```bash
npm run-script start
```

**Tests**
```
npm run-script test
```

Swagger UI is available at [/api-explorer](http://localhost:3000/api-explorer)
OpenAPI spec is available at [/api.json](http://localhost:3000/api.json)

## Environment variables

| Variable   | Default | Description                                 |
|------------|---------|---------------------------------------------|
| `API_PORT` | `3000`  | Port to listen on                           |
| `API_SEED` | `false` | Whether to seed the database with test data |


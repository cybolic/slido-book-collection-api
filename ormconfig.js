const ts = !!process.mainModule.filename.match(/\.ts$/i);

module.exports = {
   "type": "better-sqlite3",
   "database": "./data/database.db3",
   "statementCacheSize": 100,
   "synchronize": true,
   "logging": false,
   "entities": [
      ts ? "src/entities/**/*.ts" : "build/entities/**/*.js"
   ],
   "migrations": [
      ts ? "src/migration/**/*.ts" : "build/migration/**/*.js"
   ],
   "subscribers": [
      ts ? "src/subscriber/**/*.ts" : "build/subscriber/**/*.js"
   ],
   "cli": {
      "entitiesDir": ts ? "src/entities" : "build/entities",
      "migrationsDir": ts ? "src/migration" : "build/migration",
      "subscribersDir": ts ? "src/subscriber" : "build/subscriber"
   }
};

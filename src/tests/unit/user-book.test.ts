import db from '../_db'
import { getRepository } from "typeorm";
import bookEntity from '../../entities/book';
import userEntity from '../../entities/user';
import userBookEntity from '../../entities/user-book';

import {
  users as testUsers,
  books as testBooks
} from '../_data';
import { addAllBooksWithAuthors } from '../_utils';

beforeAll(() => db.setup());
afterAll(() => db.teardown());

describe("users and books are associated", () => {
  const userIndex = 0;
  let user: userEntity;
  let usersBooks = [];

  beforeAll(async () => {
    // add all books and authors
    await addAllBooksWithAuthors();

    // create a test user
    user = await getRepository(userEntity).save({ ...testUsers[userIndex] });
    // add all but last book to this user
    const booksToAdd = testBooks
      .slice(0, testBooks.length - 2);
    // ];

    // save the test user as an owner of each of the books
    await getRepository(userBookEntity).save(
      booksToAdd.map(book => ({
        userUuid: user.uuid,
        bookUuid: book.uuid
      }))
    );
    usersBooks = booksToAdd;
  });

  test("objects are created properly", () => {
    expect(user.uuid).toMatch(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
  });

  test("users can reference books", async () => {
    const usersBooksRes = await getRepository(userEntity)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.books', 'userbooks')
      .leftJoinAndMapMany('user.books', "book", "book", "book.uuid = userbooks.bookUuid")
      .where('user.username = :username', { username: user.username })
      .getMany();

    expect(usersBooksRes).toHaveLength(1);
    expect(usersBooksRes[0]).toHaveProperty('books');
    expect(usersBooksRes[0].books).toHaveLength(usersBooks.length);
    expect(usersBooksRes[0].books).toContainEqual({
      title       : usersBooks[0].title,
      description : usersBooks[0].description,
      uuid        : usersBooks[0].uuid
    });
    const lastUserBookIndex = usersBooks.length - 1;
    expect(usersBooksRes[0].books).toContainEqual({
      title       : usersBooks[lastUserBookIndex].title,
      description : usersBooks[lastUserBookIndex].description,
      uuid        : usersBooks[lastUserBookIndex].uuid
    });
  });

  test("books can reference owners / users", async () => {
    const booksOwners = await getRepository(bookEntity)
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.owners', 'userbooks')
      .leftJoinAndMapMany('book.owners', "user", "owner", "owner.uuid = userbooks.userUuid")
      .where('owner.username = :username', { username: user.username })
      .getMany();

    expect(booksOwners).toHaveLength(usersBooks.length);
    expect(booksOwners[0]).toHaveProperty('owners');
    expect(booksOwners[0].owners).toHaveLength(1);
    expect(booksOwners[0].owners[0]).toMatchObject({
      uuid: user.uuid,
      username: user.username
    });
  });

  test("books contain only actual owners", async () => {
    const booksOwners = await getRepository(bookEntity)
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.owners', 'userbooks')
      .leftJoinAndMapMany('book.owners', "user", "owner", "owner.uuid = userbooks.userUuid")
      .where('owner.uuid NOT NULL')
      .getMany();

    expect(booksOwners).toHaveLength(usersBooks.length);
    expect(booksOwners[0]).toHaveProperty('owners');
    expect(booksOwners[0].owners).toHaveLength(1);
    expect(booksOwners[0].owners[0]).toMatchObject({
      uuid: user.uuid,
      username: user.username
    });
  });

});

import db from '../_db'
import { getRepository, getConnection } from "typeorm";
import userEntity from '../../entities/user';
import { v1 as uuidv1 } from 'uuid';

beforeAll(() => db.setup());
afterAll(() => db.teardown());

const user_uuid = uuidv1();

describe("user entity", () => {
  test("can store user by name only", async () => {
    const userRepo = getConnection().getRepository(userEntity);
    // note that we're using our own upsert here; this is just for convenience
    const { identifiers : ids } = await userRepo.insert({
      username: "Jane"
    });

    const result = await getRepository(userEntity).find({
      where: {
        uuid: ids[0].uuid
      }
    });
    expect(result[0].username).toBe("Jane");
  });

  test("can store user by username and uuid", async () => {
    const userRepo = getConnection().getRepository(userEntity);
    await userRepo.save({
      username: "John",
      uuid: user_uuid
    });
    const result = await getRepository(userEntity).find({
      where: {
        uuid: user_uuid
      }
    });
    expect(result[0].username).toBe("John");
  });

  test("fails on duplicate username", async () => {
    const userRepo = getConnection().getRepository(userEntity);
    await expect(userRepo.save({
      username: "Jane"
    })).rejects.toThrowError(/UNIQUE/i);

    const result = await getRepository(userEntity).find({
      where: {
        username: "Jane"
      }
    });
    expect(result).toHaveLength(1);
  });

  test("allows update", async () => {
    const userRepo = getConnection().getRepository(userEntity);
    await expect(userRepo.save({
      uuid: user_uuid,
      username: "Doe"
    })).resolves.toHaveProperty('username', "Doe");

    // new username exists
    expect(await getRepository(userEntity).find({
      where: { username: "Doe" }
    })).toHaveLength(1);

    // old username doesn't exist
    expect(await getRepository(userEntity).find({
      where: { username: "John" }
    })).toHaveLength(0);
  });
});

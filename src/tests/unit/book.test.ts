import db from '../_db'
import { getRepository } from "typeorm";
import bookEntity from '../../entities/book';
import authorEntity from '../../entities/author';
import authorBookEntity from '../../entities/author-book';

import testData from '../_data';
import { addAllBooksWithAuthors } from '../_utils';

beforeAll(() => db.setup());
afterAll(() => db.teardown());

describe("books and authors are associated", () => {
  const bookIndex = 0;
  const authorIndex = Number(testData.books[bookIndex].authorIndex);

  beforeAll(async () => {
    await addAllBooksWithAuthors();
  });

  test("objects are created properly", () => {
    expect(testData.authors[authorIndex].uuid).toMatch(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
    expect(testData.books[bookIndex].uuid    ).toMatch(/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/);
  });

  test("authors can reference books", async () => {
    const authorsBooks = await getRepository(authorEntity)
      .createQueryBuilder('author')
      .leftJoinAndSelect('author.books', 'authorbooks')
      .leftJoinAndMapMany('author.books', "book", "book", "book.uuid = authorbooks.bookUuid")
      .where('author.lastName = :lastName', testData.authors[authorIndex])
      .getMany();

    const booksByAuthor = testData.books
      .filter(book => book.authorIndex.includes(authorIndex));

    expect(authorsBooks).toHaveLength(1);
    expect(authorsBooks[0]).toHaveProperty('books');
    expect(authorsBooks[0].books).toHaveLength(booksByAuthor.length);
    expect(authorsBooks[0].books).toContainEqual({
      title: testData.books[bookIndex].title,
      description: testData.books[bookIndex].description,
      uuid: testData.books[bookIndex].uuid
    });
  });

  test("books can reference authors", async () => {
    const booksAuthors = await getRepository(bookEntity)
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'authorbooks')
      .leftJoinAndMapMany('book.authors', "author", "author", "author.uuid = authorbooks.authorUuid")
      .where('book.title = :title', testData.books[bookIndex])
      .getMany();

    expect(booksAuthors).toHaveLength(1);
    expect(booksAuthors[0]).toHaveProperty('authors');
    expect(booksAuthors[0].authors).toHaveLength(1);
    expect(booksAuthors[0].authors[0]).toMatchObject({
      firstName: testData.authors[authorIndex].firstName,
      lastName: testData.authors[authorIndex].lastName
    });
  });

  test("books can reference multiple authors", async () => {
    const bookIndexOfMulti = Object.entries(testData.books)
      .filter(([ _, book ]) => book.authorIndex.length > 1)
      .map(([index, _]) => Number(index))
      .shift();
    const authorCount = testData.books[bookIndexOfMulti].authorIndex.length;
    const authorIndex = testData.books[bookIndexOfMulti].authorIndex[0];

    const booksAuthors = await getRepository(bookEntity)
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'authorbooks')
      .leftJoinAndMapMany('book.authors', "author", "author", "author.uuid = authorbooks.authorUuid")
      .where('book.title = :title', testData.books[bookIndexOfMulti])
      .getMany();

    expect(booksAuthors).toHaveLength(1);
    expect(booksAuthors[0]).toHaveProperty('authors');
    expect(booksAuthors[0].authors).toHaveLength(authorCount);
    expect(booksAuthors[0].authors).toContainEqual(
      testData.authors[authorIndex]
    );
  });

  test("deleting an author also removes their books", async () => {
    await getRepository(authorBookEntity).delete({ authorUuid: testData.authors[authorIndex].uuid });
    await getRepository(authorEntity).delete(testData.authors[authorIndex].uuid);

    // nothing should be found by relation
    expect(await getRepository(bookEntity)
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'authorbooks')
      .leftJoinAndMapMany('book.authors', "author", "author", "author.uuid = authorbooks.authorUuid")
      .where('author.uuid = :uuid', testData.authors[authorIndex])
      .getMany()
    ).toHaveLength(0);

    // nothing should be found by titles
    expect(await getRepository(bookEntity)
      .createQueryBuilder('book')
      .where('book.title in (:titles)', {
        titles: testData.books
          .filter(book => book.authorIndex.includes(authorIndex))
          .map(book => book.title)
      })
      .getMany()
    ).toHaveLength(0);
  });
});

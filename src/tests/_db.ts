import { createConnection, getConnection } from "typeorm";
import type { Connection } from "typeorm";
import type { BaseConnectionOptions } from "typeorm/connection/BaseConnectionOptions";

import userEntity from '../entities/user';
import authorEntity from '../entities/author';
import bookEntity from '../entities/book';
import authorBookEntity from '../entities/author-book';
import userBookEntity from '../entities/user-book';

const entitiesDefault = [ userEntity, authorEntity, bookEntity, authorBookEntity, userBookEntity ];


export async function setup ({ entities = entitiesDefault} : { entities?: BaseConnectionOptions["entities"] } = { entities: entitiesDefault } ) : Promise<Connection> {
  return createConnection({
    type: "sqljs",
    dropSchema: true,
    entities,
    synchronize: true,
    logging: false
  });
};

export async function teardown () : Promise<void> {
  const conn = getConnection();
  return await conn.close();
}

export default {
  setup, teardown
}

/*

  Test the /user account management endpoints

*/
import supertest from 'supertest';
import { getRepository } from 'typeorm';
import { v1 as uuidv1 } from 'uuid';
import { Server } from '../../server';
import db from '../_db'
import userEntity from '../../entities/user';

let server: Server;
let request: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
  const connection = await db.setup();
  server = new Server({ databaseConnection: connection });
  request = supertest(server.server.callback());
  return true;
});
afterAll(() => { server.stop(); return db.teardown() });

const user_check = {
  username: 'JaneDoe',
  uuid: uuidv1()
};

describe("routes: user", () => {
  describe("GET", () => {
    test("returns 401 for missing auth header", async () => {
      const response = await request.get('/user');

      expect(response.status).toBe(401);
      expect(response.body).toMatchObject({ message: "access denied" });
    });

    test("returns user data given a user UUID", async () => {
      await getRepository(userEntity).insert(user_check);

      const response = await request.get('/user')
        .set('api-client', user_check.uuid);

      expect(response.status).toBe(200);
      expect(response.body).toMatchObject(user_check);
    });

    test("returns 401 when auth header UUID doesn't exist", async () => {
      const uuid_success = uuidv1();
      const uuid_failure = uuidv1();
      const username = 'JohnDoe'
      await getRepository(userEntity).insert({
        uuid: uuid_success,
        username: username
      });

      expect(
        await request.get('/user').set('api-client', uuid_failure)
      ).toMatchObject({ status: 401, body: { message: "access denied" } });

      // and an extra check to make sure the success path also still works
      expect(
        await request.get('/user').set('api-client', uuid_success)
      ).toMatchObject({ status: 200, body: { uuid: uuid_success, username } });
    });
  });

  describe('POST:', () => {
    const new_user = {
      username: 'Clu',
      uuid: uuidv1()
    };

    test("can store a user", async () => {
      expect(await request.post('/user')
        .set('api-client', new_user.uuid)
        .send({ username: new_user.username })
      ).toMatchObject({ status: 201, body: { ...new_user } });

      expect(
        await request.get('/user').set('api-client', new_user.uuid)
      ).toMatchObject({ status: 200, body: { ...new_user } });
    });

    test("won't create user with existing username", async () => {
      const new_user2 = {
        username: new_user.username,
        uuid: uuidv1()
      };
      expect(await request.post('/user')
        .set('api-client', new_user2.uuid)
        .send({ username: new_user2.username })
      ).toMatchObject({ status: 404, body: { message: "username taken" } });
    });
  })

  describe('PATCH:', () => {
    test("can update a user", async () => {
      const user = {
        username: 'Tron',
        uuid: uuidv1()
      };
      const new_username = 'Rinzler';
      await getRepository(userEntity).insert(user);

      expect(await request.patch('/user')
        .set('api-client', user.uuid)
        .send({ username: new_username })
      ).toMatchObject({ status: 200, body: { username: new_username } });

      expect(
        await request.get('/user').set('api-client', user.uuid)
      ).toMatchObject({ status: 200, body: {
        ...user,
        username: new_username
      } });
    });
    test("won't update user with existing username", async () => {
      const existing_user = {
        username: 'Flynn',
        uuid: uuidv1()
      };
      const new_user = {
        username: 'Clu',
        uuid: uuidv1()
      };
      // save existing user
      await getRepository(userEntity).insert(existing_user);

      // make sure new user exists (depending on how tests are run, it might already)
      const { uuid: new_uuid } = (await getRepository(userEntity).findOne({
        where: { username: new_user.username },
        select: [ 'uuid' ]
      })) || { uuid: undefined };
      new_user.uuid = new_uuid || new_user.uuid;
      await getRepository(userEntity).save({ ...new_user });

      // now we can try the update
      expect(await request.patch('/user')
        .set('api-client', new_user.uuid)
        .send({ username: existing_user.username })
      ).toMatchObject({ status: 404, body: { message: "username taken" } });
    });
  });

  describe('DELETE:', () => {
    test("can delete a user", async () => {
      const user = {
        username: 'Cypher',
        uuid: uuidv1()
      };
      await getRepository(userEntity).insert(user);

      expect(await request.delete('/user')
        .set('api-client', user.uuid)
      ).toMatchObject({ status: 204 });

      expect(
        await request.get('/user').set('api-client', user.uuid)
      ).toMatchObject({ status: 401 });
    });
  });

});

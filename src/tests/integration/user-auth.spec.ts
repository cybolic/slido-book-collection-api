/*

  Test the basic /user security setup

*/
import supertest from 'supertest';
import { Server } from '../../server';
import db from '../_db'
import { getRepository } from 'typeorm';
import userEntity from '../../entities/user';
import { v1 as uuidv1 } from 'uuid';

import { addAllBooksWithAuthors } from '../_utils';

let server: Server;
let request: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
  const connection = await db.setup();
  await addAllBooksWithAuthors();
  server = new Server({ databaseConnection: connection });
  request = supertest(server.server.callback());
  return true;
});
afterAll(() => { server.stop(); return db.teardown() });

describe("user routes should require the 'api-client' header", () => {
  test("returns error when not given a user UUID", async () => {
    const response = await request
      .get('/user');

    expect(response.status).toBe(401);
    expect(response.body).toMatchObject({ message: "access denied" });
  });

  test("returns user data given a user UUID", async () => {
    const uuid = uuidv1();
    const username = 'JaneDoe'
    await getRepository(userEntity).insert({
      uuid: uuid,
      username: username
    });

    const response = await request
      .get('/user')
      .set('api-client', uuid);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({ uuid, username });
  });

  test("returns error when given a user UUID that doesn't exist", async () => {
    let response: supertest.Response;

    const uuid_success = uuidv1();
    const uuid_failure = uuidv1();
    const username = 'JohnDoe'
    await getRepository(userEntity).insert({
      uuid: uuid_success,
      username: username
    });

    response = await request
      .get('/user')
      .set('api-client', uuid_failure);

    expect(response.status).toBe(401);
    expect(response.body).toMatchObject({ message: "access denied" });

    // and an extra check to make sure the success path also still works
    response = await request
      .get('/user')
      .set('api-client', uuid_success);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({ uuid: uuid_success, username });
  });
});

/*

  Test the /authors endpoints

*/
import supertest from 'supertest';
import { getRepository } from 'typeorm';
import { v1 as uuidv1 } from 'uuid';
import { Server } from '../../server';
import db from '../_db'
import userEntity from '../../entities/user';
import authorEntity from '../../entities/author';

import {
  users as testUsers,
  authors as testAuthors,
  books as testBooks,
} from '../_data';
import { addAllBooksWithAuthors } from '../_utils';

let server: Server;
let request: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
  const connection = await db.setup();
  await addAllBooksWithAuthors();
  server = new Server({ databaseConnection: connection });
  request = supertest(server.server.callback());
  return true;
});
afterAll(() => { server.stop(); return db.teardown() });

// Ensure a test user exists and returns its uuid
// For quickly making an authorised request
async function getUuid () {
  const uuid = testUsers[0].uuid || uuidv1();
  await getRepository(userEntity).save({ ...testUsers[0], uuid });
  testUsers[0].uuid = uuid;
  return uuid;
}

describe("routes: author", () => {

  describe("GET", () => {
    test("returns 404 when author doesn't exist", async () => {
      const random_uuid = 'e33cd67c-a79d-11eb-b2f4-18c04d063264';
      expect(await request.get(`/authors/${random_uuid}`))
        .toMatchObject({ status: 404 });
    });
    test("can retrieve an author", async () => {
      const author = testAuthors[0];
      expect(
        await request.get(`/authors/${author.uuid}`)
      ).toMatchObject({ status: 200 });
    });
    test("can retrieve an author with their books", async () => {
      const author = testAuthors[Number(testBooks[0].authorIndex[0])];
      const booksByAuthor = testBooks
        .filter(book => book.authorIndex.includes(testBooks[0].authorIndex[0]));

      const result = await request.get(`/authors/${author.uuid}`);
      expect(result).toMatchObject({ status: 200, body: {
        uuid: author.uuid, firstName: author.firstName, lastName: author.lastName
      } });
      expect(result.body.books).toHaveLength(booksByAuthor.length);
    });
  });
  describe("POST", () => {
    test("can store an author", async () => {
      const author = { firstName: "Frank", lastName: "Herbert" }
      expect(
        await request.post(`/authors`).set('api-client', await getUuid())
          .send(author)
      ).toMatchObject({ status: 201 });

      expect(await getRepository(authorEntity).findOne(author))
      .toMatchObject(author);
    });
  });
  describe("PATCH", () => {
    test("can update an author", async () => {
      const author = {
        ...testAuthors[0],
        uuid: testAuthors[0].uuid || uuidv1()
      };
      await getRepository(authorEntity).save(author);
      testAuthors[0].uuid = author.uuid;

      expect(
        await request.patch(`/authors/${author.uuid}`).set('api-client', await getUuid())
          .send({ lastName: "Smith" })
      ).toMatchObject({ status: 200 });

      expect(await getRepository(authorEntity).findOne(author.uuid))
      .toHaveProperty('lastName', "Smith");
    });
  });
  describe("DELETE", () => {
    test("can delete an author", async () => {
      const author = testAuthors[1];
      expect(
        await request.delete(`/authors/${author.uuid}`).set('api-client', await getUuid())
      ).toMatchObject({ status: 204 });

      expect(await getRepository(authorEntity).findOne(author.uuid))
        .toBeUndefined();
    });
  });

});

/*

  Test the /user/books user collection endpoints

*/
import supertest from 'supertest';
import { getRepository } from 'typeorm';
import { v1 as uuidv1 } from 'uuid';
import { Server } from '../../server';
import db from '../_db'
import userEntity from '../../entities/user';
import userBookEntity from '../../entities/user-book';

import testData from '../_data';
import { addAllBooksWithAuthors } from '../_utils';

let server: Server;
let request: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
  const connection = await db.setup();
  await addAllBooksWithAuthors();
  server = new Server({ databaseConnection: connection });
  request = supertest(server.server.callback());
  return true;
});
afterAll(() => { server?.stop(); return db.teardown() });

// Ensure a test user exists and returns its uuid
// For quickly making an authorised request
async function getUuid () {
  const uuid = testData.users[0].uuid || uuidv1();
  await getRepository(userEntity).save({ ...testData.users[0], uuid });
  testData.users[0].uuid = uuid;
  return uuid;
}

describe("routes: user/books", () => {

  describe("POST", () => {
    test("can store a book", async () => {
      const book = testData.books[testData.books.length-1];
      // check the response
      expect(
        await request.post(`/user/books/${book.uuid}`).set('api-client', await getUuid())
      ).toMatchObject({ status: 201 });
      // also check that it was actually added to the DB
      expect(
        await getRepository(userBookEntity).findOne({
          userUuid: await getUuid(),
          bookUuid: book.uuid
        })
      ).toBeDefined();
    });
  });

  describe("DELETE", () => {
    test("can remove a book from a user's collection", async () => {
      const userUuid = await getUuid();
      const book = testData.books[testData.books.length-1];

      // make sure the book is already added
      await getRepository(userBookEntity).save({
        userUuid: userUuid,
        bookUuid: book.uuid
      });
      expect(
        await request.delete(`/user/books/${book.uuid}`).set('api-client', await getUuid())
      ).toMatchObject({ status: 204 });
    });
  });

  describe("GET", () => {
    test("can retrieve a user's array of books", async () => {
      const userUuid = await getUuid();
      const book = testData.books[testData.books.length-1];

      // make sure the book is already added
      await getRepository(userBookEntity).save({
        userUuid: userUuid,
        bookUuid: book.uuid
      });

      // check that the route says so as well
      const result = await request.get(`/user/books`).set('api-client', userUuid);
      expect(result).toMatchObject({ status: 200 });
      expect(result.body.books).toHaveLength(1);
      expect(result.body.books[0]).toMatchObject({
        title: book.title
      });
    });
  });


});

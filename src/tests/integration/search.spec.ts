/*

  Test the /search endpoint

*/
import supertest from 'supertest';
import { getRepository } from 'typeorm';
import { v1 as uuidv1 } from 'uuid';
import { Server } from '../../server';
import db from '../_db'
import userEntity from '../../entities/user';
import authorEntity from '../../entities/author';

import testData from '../_data';
import { addAllBooksWithAuthors } from '../_utils';

let server: Server;
let request: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
  const connection = await db.setup();
  await addAllBooksWithAuthors();
  server = new Server({ databaseConnection: connection });
  request = supertest(server.server.callback());
  return true;
});
afterAll(() => { server.stop(); return db.teardown() });


describe("routes: search", () => {

  function checkAuthors (authors) {
    expect(authors).toHaveLength(1);
    expect(authors[0]).toHaveProperty('lastName', "Asimov");
  }
  function checkBooks (books) {
    expect(books).toHaveLength(2);
    expect(books[0]).toHaveProperty('title');
    expect(books[1]).toHaveProperty('title');
    expect(books[0].title).toMatch(/The (Robots|Caves) of (Dawn|Steel)/);
    expect(books[1].title).toMatch(/The (Robots|Caves) of (Dawn|Steel)/);
  }

  describe("GET", () => {
    test("returns only authors when told", async () => {
      const result = await request.get('/search')
          .query({ text: 'sim', books: false });
      expect(result).toMatchObject({ status: 200 });
      expect(result.body.books).toBeUndefined();
      expect(result.body).toHaveProperty('authors');
      checkAuthors(result.body.authors);
      expect(result.body.authors[0]).toHaveProperty('books');
      checkBooks(result.body.authors[0].books);
    });

    test("returns only books when told", async () => {
      const result = await request.get('/search')
          .query({ text: 'sim', authors: false });
      expect(result).toMatchObject({ status: 200 });
      expect(result.body.authors).toBeUndefined();
      expect(result.body).toHaveProperty('books');
      checkBooks(result.body.books);
      expect(result.body.books[0]).toHaveProperty('authors');
      checkAuthors(result.body.books[0].authors);
    });

    test("returns everything by default", async () => {
      const result = await request.get('/search')
          .query({ text: 'sim' });
      expect(result).toMatchObject({ status: 200 });
      expect(result.body).toHaveProperty('authors');
      checkAuthors(result.body.authors);
      expect(result.body.authors[0]).toHaveProperty('books');
      checkBooks(result.body.authors[0].books);
      expect(result.body).toHaveProperty('books');
      checkBooks(result.body.books);
      expect(result.body.books[0]).toHaveProperty('authors');
      checkAuthors(result.body.books[0].authors);
    });
  });

});

/*

  Test the /books endpoints

*/
import supertest from 'supertest';
import { getRepository } from 'typeorm';
import { v1 as uuidv1 } from 'uuid';
import { Server } from '../../server';
import db from '../_db'
import userEntity from '../../entities/user';
import bookEntity from '../../entities/book';
import authorEntity from '../../entities/author';

import testData from '../_data';
import { addAllBooksWithAuthors } from '../_utils';

let server: Server;
let request: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
  const connection = await db.setup();
  await addAllBooksWithAuthors();
  server = new Server({ databaseConnection: connection });
  request = supertest(server.server.callback());
  return true;
});
afterAll(() => { server.stop(); return db.teardown() });

// Ensure a test user exists and returns its uuid
// For quickly making an authorised request
async function getUuid () {
  const uuid = testData.users[0].uuid || uuidv1();
  await getRepository(userEntity).save({ ...testData.users[0], uuid });
  testData.users[0].uuid = uuid;
  return uuid;
}

describe("routes: book", () => {

  describe("GET", () => {
    test("returns 404 when book doesn't exist", async () => {
      const random_uuid = 'e33cd67c-a79d-11eb-b2f4-18c04d063264';
      expect(await request.get(`/books/${random_uuid}`))
        .toMatchObject({ status: 404 });
    });
    test("can retrieve a book with its authors", async () => {
      const book = testData.books[0];
      const authors = book.authorIndex.map(index => testData.authors[index]);

      const result = await request.get(`/books/${book.uuid}`);

      expect(result).toMatchObject({ status: 200, body: { title: book.title } });
      expect(result.body.authors).toHaveLength(authors.length);
      expect(result.body.authors).toContainEqual({ firstName: authors[0].firstName, lastName: authors[0].lastName, uuid: authors[0].uuid });
    });
  });
  describe("POST", () => {
    test("can store a book", async () => {
      const book = { title: "Dune", description: "Dune is set in the distant future amidst a feudal interstellar society in which various noble houses control planetary fiefs. It tells the story of young Paul Atreides." };
      expect(
        await request.post(`/books`).set('api-client', await getUuid())
          .send(book)
      ).toMatchObject({ status: 201 });

      expect(await getRepository(bookEntity).findOne({ ...book }))
        .toHaveProperty('description', book.description);
    });
  });
  describe("PATCH", () => {
    test("can update a book", async () => {
      const book = { ...testData.books[0] };
      await getRepository(bookEntity).save(book);

      expect(await getRepository(bookEntity).findOne(book.uuid)).toHaveProperty('description', book.description);
      expect(await getRepository(bookEntity).findOne(book.uuid, { select: ['uuid'] })).toHaveProperty('uuid', book.uuid);
      expect(
        await request.patch(`/books/${book.uuid}`).set('api-client', await getUuid())
          .send({ description: "Good read" })
      ).toMatchObject({ status: 200 });

      expect(await getRepository(bookEntity).findOne(book.uuid))
        .toHaveProperty('description', "Good read");
    });
  });
  describe("DELETE", () => {
    test("can delete a book", async () => {
      const book = testData.books[1];
      expect(
        await request.delete(`/books/${book.uuid}`).set('api-client', await getUuid())
      ).toMatchObject({ status: 204 });

      expect(await getRepository(bookEntity).findOne(book.uuid))
        .toBeUndefined();
    });
  });

});

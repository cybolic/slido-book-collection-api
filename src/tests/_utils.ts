import { getConnection } from "typeorm";
import authorEntity from '../entities/author';
import bookEntity from '../entities/book';
import authorBookEntity from '../entities/author-book';

import data from './_data';

export async function addBookWithAuthors (bookIndex: number) {
  const authorRepo = getConnection().getRepository(authorEntity);
  const bookRepo = getConnection().getRepository(bookEntity);
  const authorBookRepo = getConnection().getRepository(authorBookEntity);

  const joinTable = [];
  const books = [];
  const authors = [];

  const book = data.books[bookIndex];
  books.push(book);
  for (const authorIndex of book.authorIndex) {
    const author = data.authors[authorIndex];
    if (authors.includes(author) === false) {
      authors.push(author);
    }

    joinTable.push(authorBookRepo.create({
      bookUuid: book.uuid,
      authorUuid: author.uuid
    }));
  }
  await bookRepo.save(books);
  await authorRepo.save(authors);
  await authorBookRepo.save(joinTable);
}

export async function addAllBooksWithAuthors () {
  const authorRepo = getConnection().getRepository(authorEntity);
  const bookRepo = getConnection().getRepository(bookEntity);
  const authorBookRepo = getConnection().getRepository(authorBookEntity);

  const joinTable = [];
  const books = [];
  const authors = [];
  for (const book of data.books) {
    books.push(book);
    for (const authorIndex of book.authorIndex) {
      const author = data.authors[authorIndex];
      if (authors.includes(author) === false) {
        authors.push(author);
      }

      joinTable.push(authorBookRepo.create({
        bookUuid: book.uuid,
        authorUuid: author.uuid
      }));
    }
  }
  await bookRepo.save(books);
  await authorRepo.save(authors);
  await authorBookRepo.save(joinTable)
}

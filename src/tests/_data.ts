import { v1 as uuidv1 } from 'uuid';
import userEntity from '../entities/user';
import authorEntity from '../entities/author';
import bookEntity from '../entities/book';

export const users: Partial<userEntity>[] = [
  { username: 'Count Zero', uuid: uuidv1() }
];

export const authors: Partial<authorEntity>[] & { instance?: authorEntity }[] = [
  { firstName: "Isaac", lastName: "Asimov", uuid: 'e1ac1566-a7bb-11eb-b1ac-18c04d063264' },
  { firstName: "Peter", lastName: "Watts", uuid: '6d6fc32a-a7d7-11eb-a242-18c04d063264' },
  { firstName: "Arkady", lastName: "Strugatsky", uuid: '73324e18-a7d7-11eb-b608-18c04d063264' },
  { firstName: "Boris", lastName: "Strugatsky", uuid: '7742382e-a7d7-11eb-b526-18c04d063264' }
];

type testBook = Partial<bookEntity> & { uuid: string, authorIndex: number[], instance?: bookEntity };
export const books: testBook[] = [
  {
    title: "The Robots of Dawn",
    description: "A \"whodunit\" science fiction novel by American writer Isaac Asimov, first published in 1983. It is the third novel in Asimov's Robot series.",
    authorIndex: [0], uuid: '7c06e350-a7d7-11eb-8247-18c04d063264'
  },
  {
    title: "The Caves of Steel",
    description: "A detective story that illustrates an idea Asimov advocated, that science fiction can be applied to any literary genre, rather than just being a limited genre in itself.",
    authorIndex: [0], uuid: '845a2292-a7d7-11eb-8c41-18c04d063264'
  },
  {
    title: "Blindsight",
    description: "Blindsight is a novel that examines sociopath mindsets, the consequences of automation, philosophical justifications for science, art and pursuit of knowledge, and humanity itself.",
    authorIndex: [1], uuid: '889d87c2-a7d7-11eb-9a7e-18c04d063264'
  },
  {
    title: "Roadside Picnic",
    description: "A Soviet-Russian, philosophical science fiction novel written in 1971.",
    authorIndex: [2, 3], uuid: '8ce7fbd2-a7d7-11eb-8672-18c04d063264'
  }
];

export default {
  books,
  authors,
  users
};

// Author service / model view
import { Repository, EntityRepository } from 'typeorm'
import { validate } from 'class-validator';
import Author from '../entities/author';


@EntityRepository(Author)
export default class AuthorRepository extends Repository<Author> {

  // convenience method for testing, shouldn't be necessary for real use
  async upsert (values: Partial<Author>) : Promise<Author> {
    validate(values);

    try {
      const result = await this.createQueryBuilder()
        .insert()
        .into(Author)
        .values(values)
        .orUpdate({
          conflict_target: ['firstName', 'lastName'],
          overwrite: ['firstName', 'lastName']
        })
        .execute();

      return this.findOne(result.identifiers[0].uuid);
    } catch (error) {
      console.info('sql error:', error);
    }
  }
}

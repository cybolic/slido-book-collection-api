import type { Context, Request } from 'koa';

declare global {
  type HttpMethod = 'CONNECT' | 'DELETE' | 'GET' | 'HEAD' | 'OPTIONS' | 'PATCH' | 'POST' | 'PUT' | 'TRACE';

  type OAPathDefinition = Partial<Record<HttpMethod, Function>>;

  type UserRequest = Request & { user_uuid: string };
  type UserContext = Context & { request: UserRequest, req: UserRequest };
}

import Path from 'path'
import gracefulShutdown from 'http-graceful-shutdown';
import Koa from 'koa';
import KoaLogger from 'koa-logger';
import KoaBody from 'koa-body';
import KoaRouter from 'koa-router';
import { initialize as KoaOAInit } from 'koa-openapi';
import type { KoaOpenAPIInitializeArgs } from 'koa-openapi';
import type OpenAPIFramework from 'openapi-framework';
import type * as OpenAPISecurityHandler from 'openapi-security-handler'
import type { IncomingMessage, Server as HttpServer } from 'http';

import "reflect-metadata";
import { createConnection } from "typeorm";
import type { Connection } from "typeorm";

import mountSwaggerUI from './api/swagger-ui';

import { addAllBooksWithAuthors } from './tests/_utils';

// WORKAROUND: koa-openapi doesn't mark its optional parameters as such, so we'll wrap it
type KoaOAInitArgs = Omit<KoaOpenAPIInitializeArgs, "errorMiddleware" | "securityFilter"> & {
  errorMiddleware?: KoaOpenAPIInitializeArgs["errorMiddleware"],
  securityFilter?: KoaOpenAPIInitializeArgs["securityFilter"]
};
function mountOA(args: KoaOAInitArgs): OpenAPIFramework { return KoaOAInit.call(this, args) };

interface config {
  port: string | number,
  proxy: boolean,
  databaseConnection: Connection
};
type configParameter = Partial<config>;

export class Server {
  public server: Koa<Koa.DefaultState, Koa.DefaultContext & { request: { user_uuid: string } }>;
  private router: KoaRouter;
  private config: config;
  public securityHandlers: OpenAPISecurityHandler.SecurityHandlers;

  constructor (config: configParameter = {}) {
    this.server = new Koa();
    this.router = new KoaRouter();

    gracefulShutdown(this.server);

    this.initConfiguration(config);
    this.initErrorHandling();
    this.initSecurity();
    this.initRoutes();
  }

  public async beforeShutdown () {
    if (this.config.databaseConnection) {
      console.log('Closing database connection...')
      await this.config.databaseConnection.close();
    }
  }
  public async onShutdown () {
    console.log('Server closed.')
  }

  public initConfiguration (config: configParameter = {}) {
    // Wrap our config values in an object so it's easier to replace with an
    // actual config loader in the future
    this.config = {
      port: process.env.API_PORT || 3000,
      // Auth is handled by a proxy, so forward proxied header values
      // as if they were direct
      proxy: true,
      ...(config || {}) as configParameter
    } as config;
  }

  public initErrorHandling () {
    this.server.on('error', function (error, ctx) {
      if (ctx === undefined) {
        console.error(error);
      } else {
        if (error.status >= 100 && error.status < 500) {
          ctx.status = error.status;
          ctx.body = { message: error.message };
        } else {
          console.error(error);
        }
      }
    });
    this.server.use(async (ctx, next) => {
      try {
        await next();
        // if a security handler told us to throw, do it
        if (ctx.denied !== undefined) {
          ctx.throw(ctx.denied.status, ctx.denied.message);
        }
      } catch (error) {
        ctx.app.emit('error', error, ctx);
      }
    });
  }

  public initSecurity () {
    this.server.use(async (ctx: Koa.Context, next: Koa.Next) : Promise<any> => {
      // Auth is handled by a proxy, so HTTPS requests are actually a sign
      // that a request bypassed it
      if (ctx.request.secure) {
        ctx.throw(401, "access denied");
      }
      return next();
    });
    this.securityHandlers = {
      UserSecurity: (req: IncomingMessage & { request: UserRequest }, _scopes, _definition) : boolean => {
        if (req.headers['api-client'] != undefined) {
          req.request.user_uuid = req.headers['api-client'] as string;
          return true;
        } else {
          // Normally we'd throw here, but koa-openapi isn't using async, so it
          // won't be caught; instead, we tell our middleware that _it_ should throw
          req.request.ctx.denied = { status: 401, message: "access denied" };
          // Same as above; continue to the middleware, don't throw now
          return true;
        }
      }
    };
  }

  public initRoutes () {
    // disable noise during testing
    if (process.env.NODE_ENV !== 'test') {
      this.server.use(KoaLogger());
    }
    this.server.use(KoaBody());

    mountOA({
      router: this.router,
      apiDoc: Path.join(__dirname, '..', 'api', 'v1.yaml'),
      paths: Path.join(__dirname, 'api', 'v1', 'paths'),
      // this allows us to use dependency injection on path handlers
      dependencies: {
        databaseConnection: this.config.databaseConnection
      },
      docsPath: '/api.json',
      exposeApiDocs: true,
      securityHandlers: this.securityHandlers,
      // openapi-framework expects .js files, but ts-jest/ts-node loads .ts directly 
      routesGlob: '**/*.{js,ts}',
      routesIndexFileRegExp: /(?:index)?\.(js|ts)$/
    });

    mountSwaggerUI(this.router);

    this.server.proxy = this.config.proxy;
    this.server.use(this.router.routes());
    this.server.use(this.router.allowedMethods());
  }

  public start () : HttpServer {
    const server = this.server.listen(this.config.port);
    console.log(`API server is listening on port ${this.config.port}`, )
    return server;
  }

  public async stop () {
    return this.server.removeAllListeners();
  }
}

if (require.main === module) {
  createConnection().then( async databaseConnection => {
    // seed the DB with test data
    if (process.env.API_SEED) {
      await addAllBooksWithAuthors();
    }
    const server = new Server({ databaseConnection });
    server.start();
  });
}

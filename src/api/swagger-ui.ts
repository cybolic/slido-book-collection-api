import fs from 'fs';
import * as Path from 'path';
import { absolutePath } from 'swagger-ui-dist';

import type * as Koa from 'koa';
import type * as KoaRouter from 'koa-router';

type swaggerAlias = 'index' | 'uiCss' | 'bundleJs' | 'presetJs';
type swaggerFiles = Record<swaggerAlias, string>;

const filesInfo: swaggerFiles = {
  index: 'index.html',
  uiCss: 'swagger-ui.css',
  bundleJs: 'swagger-ui-bundle.js',
  presetJs: 'swagger-ui-standalone-preset.js',
};
const files: Partial<swaggerFiles> = {};

const swaggerPath = absolutePath();
for (const [ alias, fileName ] of Object.entries(filesInfo)) {
  let content = fs.readFileSync(Path.join(swaggerPath, fileName)).toString();

  if (alias === 'index') {
    content = content.replace(
      /url: "(http|https):\/\/petstore\.swagger\.io\/v2\/swagger\.json",/,
      'configUrl: "./api-explorer/config.json",'
    );
    content = content.replace(
      /(<script src="\.\/|<link rel="stylesheet" .*?href="\.\/)(swagger-ui-bundle.js|swagger-ui-standalone-preset.js|swagger-ui.css)(")/g,
      '$1api-explorer/$2$3'
    );
  }

  files[alias] = content;
}

export default function mount (router: KoaRouter) {
  router.get('/api-explorer', (ctx: Koa.Context) => {
    ctx.type = 'text/html';
    ctx.response.body = files.index;
  });

  router.get('/api-explorer/swagger-ui.css', (ctx: Koa.Context) => {
    ctx.type = 'text/css';
    ctx.response.body = files.uiCss;
  });

  router.get('/api-explorer/swagger-ui-bundle.js', (ctx: Koa.Context) => {
    ctx.type = 'application/javascript';
    ctx.response.body = files.bundleJs;
  });

  router.get('/api-explorer/swagger-ui-standalone-preset.js', (ctx: Koa.Context) => {
    ctx.type = 'application/javascript';
    ctx.response.body = files.presetJs;
  });

  router.get('/api-explorer/config.json', (ctx: Koa.Context) => {
    ctx.response.body = {
      urls: [{ name: 'Book Collection', url: '/api.json' }],
      displayOperationId: true,
      displayRequestDuration: true,
      showExtensions: true,
      defaultModelsExpandDepth: 0
    };
  });
}

import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import userEntity from '../../../entities/user';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const userRepo = databaseConnection.getRepository(userEntity);

  async function _getUser (uuid: string) : Promise<userEntity> {
    return userRepo.findOne({ uuid });
  }


  async function GET (ctx: UserContext, _next: Koa.Next) {
    const user = await _getUser(ctx.request.user_uuid);

    if (user === undefined) {
      // We use 401 instead of 404 so the route can't be used to
      // brute-force-check whether a uuid exists
      ctx.throw(401, "access denied");
    }
    ctx.response.body = {
      username: user.username,
      uuid: user.uuid
    };
  }
  GET.apiDoc = `
    description: Get the currently authenticated user
    operationId: getUser
    security:
      - UserSecurity: []
    tags:
      - user
    responses:
      "200":
        description: "user response"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/User"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  async function POST (ctx: UserContext, _next: Koa.Next) {
    const uuid = ctx.request.user_uuid
    const user = {
      ...(ctx.request.body as { username: string }),
      uuid
    };

    try {
      await userRepo.save({
        uuid,
        username: user.username
      });
    } catch (error) {
      // non-unique username
      if (error.message.match(/UNIQUE .*username/)) {
        ctx.throw(404, "username taken");
      } else {
        console.error(error);
        ctx.throw(500, "guru meditation");
      }
    }

    ctx.status = ctx.method === 'POST'
      ? 201
      : 200;
    ctx.response.body = {
      username: user.username,
      uuid: user.uuid
    };
  }
  POST.apiDoc = `
    description: Create new user
    operationId: createUser
    security:
      - UserSecurity: []
    tags:
      - user
    parameters:
      - name: data
        in: body
        description: The user data to save
        required: false
        schema:
          $ref: "#/definitions/NewUser"
    responses:
      "201":
        description: "user response"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/User"
      "404":
        description: "username taken"
        schema:
          $ref: "#/definitions/Error"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;

  const PATCH = POST.bind({});
  PATCH.apiDoc = `${POST.apiDoc}`
    .replace(/: Create new /, ': Update')
    .replace(/: createUser/, ': updateUser')
    .replace(/The user data to save/, 'The user data to update')
    .replace(/"201":/, '"200":')


  async function DELETE (ctx: UserContext, _next: Koa.Next) {
    const uuid = ctx.request.user_uuid

    try {
      await userRepo.delete(uuid);
    } catch (error) {
      console.error("couldn't delete user:", error, error.name, error.message);
      // non-unique username
      if (Error.toString().match(/UNIQUE .*username/m)) {
        ctx.throw(401, "access denied");
      } else {
        ctx.throw(500, "guru meditation");
      }
    }

    ctx.status = 204;
  }
  DELETE.apiDoc = `
    description: Delete given user
    operationId: deleteUser
    security:
      - UserSecurity: []
    tags:
      - user
    responses:
      "204":
        description: "empty response"
      "401":
        description: "access denied"
        schema:
          $ref: "#/definitions/Error"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    GET,
    POST,
    PATCH,
    DELETE
  };
};

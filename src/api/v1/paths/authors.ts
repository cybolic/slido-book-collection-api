import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import { v1 as uuidv1 } from 'uuid';
import authorEntity from '../../../entities/author';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const authorRepo = databaseConnection.getRepository(authorEntity);


  async function GET (ctx: UserContext, _next: Koa.Next) {
    const authors = await authorRepo
      .createQueryBuilder('author')
      .getMany();

    ctx.body = {
      authors
    };
  }
  GET.apiDoc = `
    description: Get a list of authors
    operationId: getAuthors
    tags:
      - authors
    responses:
      "200":
        description: "author data"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Author"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  async function POST (ctx: UserContext, _next: Koa.Next) {
    const author : authorEntity = {
      ...(ctx.request.body as { firstName: string, lastName: string }),
      books: [],
      uuid: uuidv1()
    };

    try {
      await authorRepo.save(author);
    } catch (error) {
      // non-unique username
      if (error.message.match(/UNIQUE .*name/)) {
        ctx.throw(404, "author exists");
      } else {
        console.error(error);
        ctx.throw(500, "guru meditation");
      }
    }

    ctx.status = 201;
    ctx.response.body = {
      ...author
    };
  }
  POST.apiDoc = `
    description: Add new author
    operationId: createAuthor
    security:
      - UserSecurity: []
    tags:
      - authors
    parameters:
      - name: data
        in: body
        description: The author data to save
        required: true
        schema:
          $ref: "#/definitions/NewAuthor"
    responses:
      "201":
        description: "author response"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Author"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    GET,
    POST
  };
};

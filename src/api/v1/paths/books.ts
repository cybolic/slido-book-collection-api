import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import { v1 as uuidv1 } from 'uuid';
import bookEntity from '../../../entities/book';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const bookRepo = databaseConnection.getRepository(bookEntity);

  async function POST (ctx: UserContext, _next: Koa.Next) {
    const book : bookEntity = {
      ...ctx.request.body,
      authors: [],
      uuid: uuidv1()
    };

    if (await bookRepo.findOne(book.uuid, { select: ['uuid'] }) != null) {
      ctx.status = 200
      ctx.body = "book already added";
    } else {
      await bookRepo.save(book);
      ctx.status = 201;
    }

  }
  POST.apiDoc = `
    description: Add new book
    operationId: createBook
    security:
      - UserSecurity: []
    tags:
      - books
    parameters:
      - name: data
        in: body
        description: The book data to save
        required: true
        schema:
          $ref: "#/definitions/BaseBook"
    responses:
      "201":
        description: "book response"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    POST
  };

}

import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import bookEntity from '../../../../entities/book';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const bookRepo = databaseConnection.getRepository(bookEntity);


  async function GET (ctx: UserContext, _next: Koa.Next) {
    const userUuid = ctx.request.user_uuid;
    const ownedBooks = await bookRepo
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.owners', 'userbooks')
      .leftJoinAndSelect('book.authors', 'authorbooks')
      .leftJoinAndMapMany('book.owners', "user", "owner", "owner.uuid = userbooks.userUuid")
      .leftJoinAndMapMany('book.authors', "author", "author", "author.uuid = authorbooks.authorUuid")
      .where('owner.uuid = :uuid', { uuid: userUuid })
      .getMany();

    ctx.body = {
      books: (Object.entries(ownedBooks) || []).map(([ _, book ]) => ({
        uuid: book.uuid,
        title: book.title,
        description: book.description,
        authors: book.authors 
      }))
    };
  }
  GET.apiDoc = `
    description: Get user's books
    operationId: getUserBooks
    tags:
      - collection
    security:
      - UserSecurity: []
    responses:
      "200":
        description: "book data"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Book"
      "404":
        description: "not found"
        schema:
          $ref: "#/definitions/Error"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    GET
  };

}

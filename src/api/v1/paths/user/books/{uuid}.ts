import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import bookEntity from '../../../../../entities/book';
import userBookEntity from '../../../../../entities/user-book';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const bookRepo = databaseConnection.getRepository(bookEntity);
  const userBookRepo = databaseConnection.getRepository(userBookEntity);


  async function POST (ctx: UserContext, _next: Koa.Next) {
    const uuid = ctx.request.user_uuid;
    const bookUuid = ctx.params.uuid;

    if (await bookRepo.findOne(bookUuid, { select: ['uuid'] }) == null) {
      ctx.throw(404, "book not found");
    }
    if (await userBookRepo.findOne({
      userUuid: uuid,
      bookUuid
    })) {
        ctx.throw(200, "book already added");
    } else {
      await userBookRepo.save({
        userUuid: uuid,
        bookUuid: bookUuid
      });
    }

    ctx.status = 201;
  }
  POST.apiDoc = `
    description: Save book to user collection
    operationId: saveUserBook
    security:
      - UserSecurity: []
    tags:
      - collection
    parameters:
      - name: uuid
        in: path
        description: The UUID of the book to add
        required: true
        type: string
    responses:
      "201":
        description: "book response"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  async function DELETE (ctx: UserContext, _next: Koa.Next) {
    const uuid = ctx.request.user_uuid;
    const bookUuid = ctx.params.uuid;

    if (await bookRepo.findOne(bookUuid, { select: ['uuid'] }) == null) {
      ctx.throw(404, "book not found");
    }
    if (await userBookRepo.findOne({
      userUuid: uuid,
      bookUuid
    })) {
      await userBookRepo.delete({
        userUuid: uuid,
        bookUuid: bookUuid
      });
      ctx.status = 204;
    } else {
        ctx.throw(404, "user does not own book");
    }
  }
  DELETE.apiDoc = `
    description: Remove a book from a user's collection
    operationId: removeUserBook
    security:
      - UserSecurity: []
    tags:
      - collection
    parameters:
      - name: uuid
        in: path
        description: The UUID of the book to remove
        required: true
        type: string
    responses:
      "204":
        description: "empty response"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    POST,
    DELETE
  };

}

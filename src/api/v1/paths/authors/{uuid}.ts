import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import authorEntity from '../../../../entities/author';
import authorBookEntity from '../../../../entities/author-book';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const authorRepo = databaseConnection.getRepository(authorEntity);
  const authorBookRepo = databaseConnection.getRepository(authorBookEntity);

  async function GET (ctx: UserContext, _next: Koa.Next) {
    const authorUuid = ctx.params.uuid;
    const author = await authorRepo
      .createQueryBuilder('author')
      .leftJoinAndSelect('author.books', 'authorbooks')
      .leftJoinAndMapMany('author.books', "book", "book", "book.uuid = authorbooks.bookUuid")
      .where('author.uuid = :uuid', { uuid: authorUuid })
      .getOne();

    if (author === undefined) {
      ctx.throw(404, "author not found");
    }
    ctx.response.body = {
      ...author
    };
  }
  GET.apiDoc = `
    description: Get data on an author
    operationId: getAuthor
    tags:
      - authors
    parameters:
      - name: uuid
        in: path
        description: The UUID of the author to look up
        required: true
        type: string
    responses:
      "200":
        description: "author data"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Author"
      "404":
        description: "not found"
        schema:
          $ref: "#/definitions/Error"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  async function PATCH (ctx: UserContext, _next: Koa.Next) {
    const author = {
      ...(ctx.request.body as { firstName: string, lastName: string }),
      uuid: ctx.params.uuid
    };

    try {
      await authorRepo.save(author);
    } catch (error) {
      // non-unique username
      if (error.message.match(/UNIQUE .*name/)) {
        ctx.throw(404, "author exists");
      } else {
        console.error(error);
        ctx.throw(500, "guru meditation");
      }
    }

    if (ctx.method === 'POST') {
      ctx.status = 201;
      ctx.response.body = {
        ...author
      };
    } else {
      ctx.status = 200;
      ctx.response.body = {
        ...ctx.request.body
      };
    }
  }
  PATCH.apiDoc = `
    description: Update author
    operationId: updateAuthor
    security:
      - UserSecurity: []
    tags:
      - authors
    parameters:
      - name: uuid
        in: path
        description: The UUID of the author to update
        required: true
        type: string
      - name: data
        in: body
        description: The author data to update
        required: false
        schema:
          $ref: "#/definitions/UpdateAuthor"
    responses:
      "200":
        description: "author response"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Author"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  async function DELETE (ctx: UserContext, _next: Koa.Next) {
    const authorUuid = ctx.params.uuid;

    if (await authorRepo.findOne(authorUuid, { select: ['uuid'] }) == null) {
      ctx.throw(404, "author not found");
    }
    if (await authorBookRepo.findOne({ authorUuid })) {
      await authorBookRepo.delete({ authorUuid });
    }
    await authorRepo.delete(authorUuid);
    ctx.status = 204;
  }
  DELETE.apiDoc = `
    description: Remove an author
    operationId: removeAuthor
    security:
      - UserSecurity: []
    tags:
      - authors
    parameters:
      - name: uuid
        in: path
        description: The UUID of the author to remove
        required: true
        type: string
    responses:
      "204":
        description: "empty response"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    GET,
    PATCH,
    DELETE
  };
};

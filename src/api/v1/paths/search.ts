import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import authorEntity from '../../../entities/author';
import bookEntity from '../../../entities/book';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const authorRepo = databaseConnection.getRepository(authorEntity);
  const bookRepo = databaseConnection.getRepository(bookEntity);

  async function GET (ctx: UserContext, _next: Koa.Next) {
    const {
      text: searchText,
      authors: searchAuthors = true,
      books: searchBooks = true
    } = ctx.request.query as unknown as { text: string, authors: boolean, books: boolean };

    let bookResults : bookEntity[] = [];
    let authorResults : authorEntity[] = [];

    if (searchAuthors) {
     authorResults = await authorRepo
      .createQueryBuilder('author')
      .leftJoinAndSelect('author.books', 'authorbooks')
      .leftJoinAndMapMany('author.books', "book", "book", "book.uuid = authorbooks.bookUuid")
      .where(`UPPER(author.firstName || ' ' || author.lastName) LIKE '%' || :text || '%'`, { text: searchText.toUpperCase() })
      .getMany();
  }

    if (searchBooks) {
      bookResults = await bookRepo
        .createQueryBuilder('book')
        .leftJoinAndSelect('book.owners', 'userbooks')
        .leftJoinAndSelect('book.authors', 'authorbooks')
        .leftJoinAndMapMany('book.owners', "user", "owner", "owner.uuid = userbooks.userUuid")
        .leftJoinAndMapMany('book.authors', "author", "author", "author.uuid = authorbooks.authorUuid")
        .where(`UPPER(book.title) LIKE '%' || :text || '%'`, { text: searchText.toUpperCase() })
        .orWhere(`UPPER(book.description) LIKE '%' || :text || '%'`, { text: searchText.toUpperCase() })
        .getMany();
    }

    ctx.response.body = {
      ...(
        authorResults.length
          ? { authors: authorResults }
          : {}
      ),
      ...(
        bookResults.length
          ? { books: bookResults }
          : { }
      )
    };
  }
  GET.apiDoc = `
    description: Search for an author or book
    operationId: search
    tags:
      - search
    parameters:
      - name: text
        in: query
        description: What to search for
        required: true
        type: string
        minLength: 3
        maxLength: 255
      - name: authors
        in: query
        description: Search authors
        type: boolean
        default: true
      - name: books
        in: query
        description: Search books
        type: boolean
        default: true
    responses:
      "200":
        description: "search results"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/SearchResult"
      "404":
        description: "not found"
        schema:
          $ref: "#/definitions/Error"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    GET
  };
};

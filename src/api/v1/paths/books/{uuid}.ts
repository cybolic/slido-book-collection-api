import type * as Koa from 'koa';
import type { Connection } from 'typeorm'
import bookEntity from '../../../../entities/book';
import authorBookEntity from '../../../../entities/author-book';

export default function (databaseConnection: Connection) : OAPathDefinition {
  const bookRepo = databaseConnection.getRepository(bookEntity);
  const authorBookRepo = databaseConnection.getRepository(authorBookEntity);


  async function GET (ctx: UserContext, _next: Koa.Next) {
    const bookUuid = ctx.params.uuid;
    const book: bookEntity = await bookRepo
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'authorbooks')
      .leftJoinAndMapMany('book.authors', "author", "author", "author.uuid = authorbooks.authorUuid")
      .where('book.uuid = :uuid', { uuid: bookUuid })
      .getOne();

    if (book === undefined) {
      ctx.throw(404, "book not found");
    }
    ctx.body = {
      ...book
    };
  }
  GET.apiDoc = `
    description: Get data on a book
    operationId: getBook
    tags:
      - books
    parameters:
      - name: uuid
        in: path
        description: The UUID of the book to look up
        required: true
        type: string
    responses:
      "200":
        description: "book data"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Book"
      "404":
        description: "not found"
        schema:
          $ref: "#/definitions/Error"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  async function PATCH (ctx: UserContext, _next: Koa.Next) {
    const bookUuid = ctx.params.uuid;
    const book : bookEntity = {
      ...ctx.request.body
    };

    if (await bookRepo.findOne(bookUuid, { select: ['uuid'] }) == null) {
      ctx.throw(404, "book not found");
    }

    await bookRepo.update(bookUuid, book);
    ctx.status = 200;
  }
  PATCH.apiDoc = `
    description: Update a book
    operationId: updateBook
    security:
      - UserSecurity: []
    tags:
      - books
    parameters:
      - name: uuid
        in: path
        description: The UUID of the book to update
        required: true
        type: string
      - name: data
        in: body
        description: The book data to update
        required: true
        schema:
          $ref: "#/definitions/BaseBook"
    responses:
      "200":
        description: "book response"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  async function DELETE (ctx: UserContext, _next: Koa.Next) {
    const bookUuid = ctx.params.uuid;

    if (await bookRepo.findOne(bookUuid, { select: ['uuid'] }) == null) {
      ctx.throw(404, "book not found");
    }

    if (await authorBookRepo.findOne({ bookUuid })) {
      await authorBookRepo.delete({ bookUuid });
    }
    await bookRepo.delete(bookUuid);
    ctx.status = 204;
  }
  DELETE.apiDoc = `
    description: Delete a book
    operationId: deleteBook
    security:
      - UserSecurity: []
    tags:
      - books
    parameters:
      - name: uuid
        in: path
        description: The UUID of the book to delete
        required: true
        type: string
    responses:
      "204":
        description: "empty response"
      default:
        description: "unexpected error"
        schema:
          $ref: "#/definitions/Error"
  `;


  return {
    GET,
    PATCH,
    DELETE
  };

}

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Length, IsUUID } from 'class-validator';
import User from './user';
import Author from './author';
import AuthorBook from './author-book';
import UserBook from './user-book';

@Entity()
export default class Book {

  @PrimaryGeneratedColumn('uuid')
  @IsUUID()
  uuid: string;

  // the longest English book title is 5820 chars, longest non-English is 26021
  // but Amazon cuts titles longer than 160, so let's just one-up them by 16
  @Column({ type: 'varchar', length: 176 })
  @Length(1, 176)
  title: string;

  // no real reason for 4096 other than that it's half of README.md, which is
  // too long already
  @Column({ type: 'text', length: 4096 })
  @Length(3, 4096)
  description: string

  // @ManyToMany(() => Author, author => author.books)
  @OneToMany(() => AuthorBook, authorbook => authorbook.book)
  authors: Author[];

  // @ManyToMany(() => User, user => user.books)
  @OneToMany(() => UserBook, userbook => userbook.book)
  owners: User[];

}

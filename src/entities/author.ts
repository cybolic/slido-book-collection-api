import { Entity, PrimaryGeneratedColumn, Column, Unique, OneToMany } from "typeorm";
import { Length, IsUUID } from 'class-validator';
import Book from './book';
import AuthorBook from './author-book';

@Entity()
// in a real project, name alone should not be enough to be unique, but probably birthdate, country, etc. as well
@Unique('name_index', ['firstName', 'lastName'])
export default class Author {

  @PrimaryGeneratedColumn('uuid')
  @IsUUID()
  uuid: string;

  @Column({ type: 'varchar', length: 150 })
  @Length(1, 150)
  firstName: string;

  @Column({ type: 'varchar', length: 150 })
  @Length(1, 150)
  lastName: string;

  // @ManyToMany(() => Book, book => book.authors, { onDelete: 'CASCADE' })
  // @JoinTable({
  //   name: 'author_books',
  //   joinColumn: { name: 'author', referencedColumnName: 'uuid' },
  //   inverseJoinColumn: { name: 'book', referencedColumnName: 'uuid' }
  // })
  @OneToMany(() => AuthorBook, authorbook => authorbook.author)
  books: Book[];

}

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Length, IsUUID } from 'class-validator';
import Book from './book';
import UserBook from './user-book';

@Entity()
export default class User {

  @PrimaryGeneratedColumn('uuid')
  @IsUUID()
  uuid: string;

  @Column({ type: 'varchar', length: 150, unique: true })
  @Length(1, 150)
  username: string;

  // @ManyToMany(() => Book, book => book.owners)
  // @JoinTable({
  //   name: 'user_books',
  //   joinColumn: { name: 'user', referencedColumnName: 'uuid' },
  //   inverseJoinColumn: { name: 'book', referencedColumnName: 'uuid' }
  // })
  @OneToMany(() => UserBook, userbook => userbook.owner)
  books: Book[];


}

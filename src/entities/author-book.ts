import { Entity, PrimaryColumn, ManyToOne, JoinColumn, Unique } from "typeorm";
import Author from './author';
import Book from './book';

@Entity()
@Unique('pair_index', ['authorUuid', 'bookUuid'])
export default class AuthorBook {

  @PrimaryColumn('uuid')
  authorUuid: string;

  @PrimaryColumn('uuid')
  bookUuid: string;

  @ManyToOne(() => Author, author => author.books, { primary: true })
  @JoinColumn({ name: 'authorUuid' })
  author: Author[];

  @ManyToOne(() => Book, book => book.authors, { primary: true })
  @JoinColumn({ name: 'bookUuid' })
  book: Book[];
}

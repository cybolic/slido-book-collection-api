import { Entity, PrimaryColumn, ManyToOne, JoinColumn } from "typeorm";
import User from './user';
import Book from './book';

@Entity()
export default class UserBook {

  @PrimaryColumn('uuid')
  userUuid: string;

  @PrimaryColumn('uuid')
  bookUuid: string;

  @ManyToOne(() => User, user => user.books, { primary: true })
  @JoinColumn({ name: 'userUuid' })
  owner: User[];

  @ManyToOne(() => Book, book => book.owners, { primary: true })
  @JoinColumn({ name: 'bookUuid' })
  book: Book[];
}
